// Variables
var modal = document.getElementById("confirmModal");
var content; 
var plant
var jobJASON

// Event listener for the submit button + Validation Script (TODO: refactor validation into its own functio)
document.getElementById("submitBtn").addEventListener("click",function(e){
     e.preventDefault();

// Clear error messages on submit
    document.getElementById("error1").innerHTML= "";   
    document.getElementById("error2").innerHTML= "";
    document.getElementById("error3").innerHTML= "";
    document.getElementById("error4").innerHTML= "";
    document.getElementById("error5").innerHTML= "";
    document.getElementById("error6").innerHTML= "";

// Get data from the form.
    let plant = "P1" /* Temp for test purposes.*/
    let zone = getSelections("zone");
    let aisle = getSelections("aisle");
    let section = getSelections("section");
    let shelf = getSelections("shelf");
    let binCount = document.getElementById("binCount").value;
    let binStart = document.getElementById("binStart").value;
    let printIp = document.getElementById("printerIp").value;
    let isError = false;
// Validate form data
    if(zone == ""){
        document.getElementById("error1").innerHTML = "<p>Required</p>"
        isError = true;
    }
    if(aisle == null){
        document.getElementById("error2").innerHTML = "<p>Required</p>"
        isError = true;
    }
    if(section == null){
        document.getElementById("error3").innerHTML = "<p>Required</p>"
        isError = true;
    }
    if(shelf == null){
        document.getElementById("error4").innerHTML = "<p>Required</p>"
        isError = true;
    }
    if(binCount == ""){
        document.getElementById("error5").innerHTML += "<p>Please enter a count</p> <br/>"
        isError = true;
    }
    if(binStart == ""){
        document.getElementById("error5").innerHTML += "<p>Please enter a starting bin</p>"
        isError = true;
    }
    if(printIp == null){
        document.getElementById("error6").innerHTML = "<p>Please enter target printer's IP Address</p>"
        isError = true;
    }
    if(isDottedIPv4(printIp) == false){
        document.getElementById("error6").innerHTML = "<p>Incorrectly formatted IP Address</p>"
        isError = true;
    }
    if (isError == false)
    {
        const printJob = {facility: plant, zones: zone, aisles: aisle, sections: section, shelves: shelf, counts: binCount, startingBin: binStart, printerIP: printIp};
        jobJASON = JSON.stringify(printJob);
        openModal(jobJASON);
        //openModal("<h1>Job Confirmation</h1><br/><p>Are you sure you wish to print X labels?</br>");

    }
});
// Validation function for IPv4 - Code Origin: https://stackoverflow.com/questions/42345/how-to-evaluate-an-ip
function isDottedIPv4(s)
   {
    var match = s.match(/^(\d+)\.(\d+)\.(\d+)\.(\d+)$/);
    return match != null &&
           match[1] <= 255 && match[2] <= 255 &&
           match[3] <= 255 && match[4] <= 255;
}
// Function to display the modal
function openModal(string) {
    modal.style.display = "inline-block";
    document.getElementById("confirmationMessage").innerHTML=string; 
}
// XMLHttpRequest Function - need to get directory information fron Nick.
function sendData(string){
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "labelFactory.py", true);
    xhr.send(string);
    document.getElementById("error1").innerHTML= xhr.responseText;
 
}
// Function to get all selected options in the multi-select inputs.
function getSelections(string){
    var selectedItems = [];
    for (var option of document.getElementById(string).options)
    {
        if (option.selected) {
            selectedItems.push(option.value)
        }
    }
    // Check that selections were made - if so return array, otherwise return null.  Should probably be a seperate validation method in future refactoring.    
    if (selectedItems.length >0)
    {
        return selectedItems;
    }
    else 
    {
        return null;
    }
}

// Event listener for the cancel job button.
document.getElementById("cancelJobBtn").addEventListener("click", function(){
    modal.style.display = "none";
    document.getElementById("resetBtn").click();
});

// Event listener for the confirm job button.
document.getElementById("confirmJobBtn").addEventListener("click", function(){
    modal.style.display = "none";
    sendData(jobJASON);
    document.getElementById("resetBtn").click();
});


